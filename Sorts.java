public class Sorts {
	
	//Sorts the list of size elements contained in arr using the selection sort algorithm.
	public static <Type extends Comparable<? super Type>> void selectionSort(Type[] arr, int size) {
		
		Type min;
		Type temp;
		int minIndex;
		
		for(int i=0; i<size; i++) {
			
			min = arr[i];
			minIndex = i;
			
			for(int j=i+1; j<size; j++) {
				
				if(min.compareTo(arr[j]) > 0 ) {
					min = arr[j];
					minIndex = j;
				}
				
			}
			
			temp = arr[i];
			arr[i] = min;
			arr[minIndex] = temp;
			
		}
		
	} 
	
	//Sorts the list of size elements contained in arr using the bubble sort algorithm.
	public static <Type extends Comparable<? super Type>> void bubbleSort(Type[] arr, int size) {
		
		int left;
		int right;
		Type temp;
		
		for(int i=1; i<size; i++) {
						
			for(int j=0; j<size-i; j++) {
			
				left = j;
				right = j + 1;
			
				if(arr[left].compareTo(arr[right]) > 0) {
				
					temp = arr[left];
					arr[left] = arr[right];
					arr[right] = temp;
				
				}
				
			}
			
		}
		
	}
	
	//Sorts the list of size elements contained in arr using the insertion sort algorithm.
	public static <Type extends Comparable<? super Type>> void insertionSort(Type[] arr, int size) {
		
		Type temp;
		int j;
		
		for(int i=1; i<size; i++) {
			
			temp = arr[i];
			j = i;
			
			while(j>0 && arr[j-1].compareTo(temp) > 0) { 				
			
				arr[j] = arr[j-1];
				j--;
			
			}
			arr[j] = temp;
		
		}
		
	}
	
	//Sorts the list of size elements contained in arr using the merge sort algorithm.
	public static <Type extends Comparable<? super Type>> void mergeSort(Type[] arr, int size) {
		
		mergeSort(arr,0,size-1);
		
	}
	
	private static <Type extends Comparable<? super Type>> void mergeSort(Type[] arr, int first, int last) {
		
		int middle;
		
		if(first < last) {
			
			middle = (first + last) / 2;
			mergeSort(arr,first,middle);
			mergeSort(arr,middle+1,last);
			mergeSortedHalves(arr,first,middle,last);

		}
		
	}
	
	private static <Type extends Comparable<? super Type>> void mergeSortedHalves(Type[] arr, int left, int middle, int right) {
		
		Type[] temp = (Type[]) new Comparable[right-left+1];
		int index1 = left;
		int index2 = middle+1;
		int index = 0;
		
		while(index1 <= middle && index2 <= right) {
				
			if(arr[index1].compareTo(arr[index2]) < 0) {
				temp[index] = arr[index1];
				index1++;
			}
			else {
				temp[index] = arr[index2];
				index2++;
			}
			index++;
			
		}
		
		if(index1<=middle) {
			
			while(index1<=middle) {
				temp[index] = arr[index1];
				index++;
				index1++;	
			}
			
		}
		else {
			
			while(index2<=right) {
				temp[index] = arr[index2];
				index++;
				index2++;	
			}
			
		}
		
		//Copy elements from temp location back to array	
		for(int i=0; i<temp.length; i++)
			arr[left+i] = temp[i];	
		
	}
		 
	//Sorts the list of size elements contained in arr using the quick sort algorithm withmedian-of-three pivot and rearrangement of the three elements (as done in class).
	public static <Type extends Comparable<? super Type>> void quickSort(Type[] arr, int size) {
		
		quickSort(arr,0,size-1);	
		
	}
	
	private static <Type extends Comparable<? super Type>> void quickSort(Type[] arr, int first, int last) {
		
		int pivotIndex;
		
		if(first < last) {
		
			setPivotToEnd(arr,first,last);
			pivotIndex = splitList(arr,first,last);
			quickSort(arr,first,pivotIndex-1);
			quickSort(arr,pivotIndex+1,last);
		
		}
		
	}
	
	private static <Type extends Comparable<? super Type>> void setPivotToEnd(Type[] arr, int left, int right) {
		
		int center = (left + right) / 2;
		Type temp;
		
		if(arr[center].compareTo(arr[left]) < 0) {
			
			temp = arr[center];
			arr[center] = arr[left];
			arr[left] = temp;
			
		}
		if(arr[right].compareTo(arr[left]) < 0) {
			
			temp = arr[right];
			arr[right] = arr[left];
			arr[left] = temp;
			
		}
		if(arr[center].compareTo(arr[right]) < 0) {
			
			temp = arr[center];
			arr[center] = arr[right];
			arr[right] = temp;
			
		} 
		
	}
	
	private static <Type extends Comparable<? super Type>> int splitList(Type[] arr, int left, int right) {
		
		int indexL = left;
		int indexR = right-1;
		Type pivot = arr[right];
		Type temp;
		
		//While indices didn't cross over
		while(indexL <= indexR) {
			
			while(arr[indexL].compareTo(pivot) < 0) 	
				indexL++;
			
			while(indexL <= indexR && arr[indexR].compareTo(pivot) > 0)
				indexR--;
			
			if(indexL <= indexR) {
				
				temp = arr[indexL];
				arr[indexL] = arr[indexR];
				arr[indexR] = temp;
				indexL++;
				indexR--;
				
			}
			
		}
		
		//Swap element at indexL with pivot
		temp = arr[indexL];
		arr[indexL] = arr[right];
		arr[right] = temp;
		
		return indexL;
		
	}
	
}