import java.util.*;

public class SortTest {
	
	public static void main(String[] args) {
		
		int size = 10;
		Integer[] arr = new Integer[size];
		Random rand = new Random();
		
		//Generate Random numbers for array
		for(int i=0; i<size; i++) {
			arr[i] = rand.nextInt(size);
			System.out.print(arr[i] + " ");
		}		
		
		//Sort
		//Sorts.selectionSort(arr,size);
		//Sorts.bubbleSort(arr,size);
		//Sorts.insertionSort(arr,size);
		//Sorts.mergeSort(arr,size);
		Sorts.quickSort(arr,size);
		
		//Print Sorted Array
		System.out.println("\n");
		for(int i=0; i<size; i++) {
			System.out.print(arr[i] + " ");
		}
		
	}
	
}