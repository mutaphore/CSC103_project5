public class SortTimes1 {

	public static void main(String[] args) {
	
		int size = 80000;
		Integer[] arr1 = new Integer[size];
		Integer[] arr2 = new Integer[size];
		Integer[] arr3 = new Integer[size];
		Integer[] arr4 = new Integer[size];
		Integer[] arr5 = new Integer[size];
		long startTime;
		long endTime;
		long time1,time2,time3,time4,time5;
		
		System.out.println("TEST1: presorted list\n");
		
		for(int N=5000; N<=size; N*=2) {
			
			//Generate a presorted list of numbers
			for(int i=0; i<N; i++) {
				
				arr1[i] = i;
				arr2[i] = i;
				arr3[i] = i;
				arr4[i] = i;
				arr5[i] = i;
				
			}
			
            startTime = System.nanoTime();
			Sorts.selectionSort(arr1,N);
			endTime = System.nanoTime();
			time1 = (endTime - startTime);
			
            startTime = System.nanoTime();
			Sorts.bubbleSort(arr2,N);
			endTime = System.nanoTime();
			time2 = (endTime - startTime);
			
            startTime = System.nanoTime();
			Sorts.insertionSort(arr3,N);
			endTime = System.nanoTime();
			time3 = (endTime - startTime);
			
            startTime = System.nanoTime();
			Sorts.mergeSort(arr4,N);
			endTime = System.nanoTime();
			time4 = (endTime - startTime);
			
            startTime = System.nanoTime();
			Sorts.quickSort(arr5,N);
			endTime = System.nanoTime();
			time5 = (endTime - startTime);
			
			System.out.println("N=" + N + ": T_ss=" + time1 + ", T_bs=" + time2 + ", T_is=" + time3 + ", T_ms=" + time4 + ", T_qs=" + time5);
			
		}
		
		System.out.println("\nEND TEST 1");
		
	}

}